'use strict';

var prompt = require('readline-sync').question,
    random = Math.random,
    log = console.log,
    validChoices = ['rock', 'scissors', 'paper'],
    question = 'Do you choose rock, paper, or scissors? ';

log((function playRound(userChoice, computerChoice) {
  userChoice = userChoice.toLowerCase();
  if (!~validChoices.indexOf(userChoice)) return (log('Invalid choice: ' + userChoice), playRound(prompt(question), computerChoice));
  computerChoice = computerChoice < 0.34 && 'rock' || computerChoice <= 0.67 && 'paper' || 'scissors';
  log('Computer has chosen ' + computerChoice + '.');
  return userChoice === computerChoice && 'The result is a tie!' ||
    userChoice === 'paper' && (computerChoice === 'rock' && 'You win with paper.' || 'Computer wins with scissors.') ||
    userChoice === 'rock' && (computerChoice === 'paper' && 'Computer wins with paper.' || 'You win with rock.') ||
    computerChoice === 'paper' && 'You win with scissors.' || 'Computer wins with rock.';
})(prompt(question), random()));
