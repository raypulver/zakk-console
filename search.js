var myName = 'Eric';
var text = 'Blah blah blah blah blah blah Eric blah blah blah Eric blah blah Eric blah blah blah blah blah blah blah Eric';
var hits = text.match(RegExp(myName, 'g'));

if (hits.length === 0) {
	console.log("Your name wasn't found!");
} else {
	console.log(hits);
}
